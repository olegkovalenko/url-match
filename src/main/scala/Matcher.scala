import java.net.URL

object Matcher {
  val hostRegex = """host\((.*)\)""".r
  val pathRegex = """path\((.*)\)""".r
  val queryParamRegex = """queryparam\((.*)=(.*)\)""".r
  def apply(query: String) = new Matcher(extractPatterns(query))
  def extractPatterns(query: String): Array[Pattern] = {
    query.split(";").map(_.stripPrefix(" ").stripSuffix(" ")).map {
      case hostRegex(domain) => HostP(strToPattern(domain))
      case pathRegex(path) => PathP(path.split("/").map(strToPattern))
      case queryParamRegex(name, value) => QueryParamP(name, strToPattern(value))
      case _ @ clause => throw new RuntimeException(s"Unknown pattern '$clause'")
    }
  }
  def strToPattern(str: String): Pattern = if (str.startsWith("?")) BindP(str.substring(1)) else ConstP(str)

}
sealed class Pattern
case class ConstP(value: String) extends Pattern
case class BindP(name: String) extends Pattern
case class HostP(domain: Pattern) extends Pattern
case class PathP(parts: Array[Pattern]) extends Pattern
case class QueryParamP(name: String, value: Pattern) extends Pattern

case class Host(domain: String)
case class Path(parts: Array[String])
case class QueryParam(name: String, value: String)
case class UrlValue(host: Host, path: Path, params: Array[QueryParam])


class Matcher(val patterns: Array[Pattern]) {
  def recognize(url: String): Option[Map[String, String]] = {
    val parsedUrl = new URL(url)
    val value = UrlValue(
      Host(parsedUrl.getHost),

      Path((if (parsedUrl.getPath == "") "/" else parsedUrl.getPath()).split("/").drop(1)),

      Option(parsedUrl.getQuery()).map { query =>
        query.split("&").foldLeft(Array[QueryParam]()) { (m, kv) =>
          kv.split("=") match {
            case Array(k, v) => m :+ QueryParam(k, v)
            case Array(k)    => m :+ QueryParam(k, "")
          }
        }
      }.getOrElse(Array())
    )
    val acc: Option[Map[String, String]] = Some(Map[String, String]())
    patterns.foldLeft(acc)(extract(value))
  }

  type Env = Map[String, String]
  type OptEnv = Option[Env]

  def extract(urlValue: UrlValue)(acc: OptEnv, pattern: Pattern): OptEnv = (acc, pattern) match {
    case (acc @ None, _) => acc
    case (acc @ Some(binds), pattern) => pattern match {
      case HostP(ConstP(host)) => if (host == urlValue.host.domain) acc else None
      case HostP(BindP(name))  => Some(binds.updated(name, urlValue.host.domain))
      case PathP(parts) =>
        if (parts.size == urlValue.path.parts.size) {
          parts.zip(urlValue.path.parts).foldLeft(acc: OptEnv) {
            case (None                , _) => None
            case (optEnv @ Some(_)    , (ConstP(name)   , piece)) => if (name == piece) optEnv else None
            case (optEnv @ Some(binds), (BindP(bindName), piece)) => Some(binds.updated(bindName, piece))
            case (_                   , (unexpected     , piece)) => throw new RuntimeException(s"Unexpected pattern $unexpected for path $parts at $piece")
          }
        } else {
          None
        }
      case QueryParamP(k, ConstP(v)) =>
        urlValue.params.find { case QueryParam(name, value) => k == name && v == value } flatMap { _ => acc } orElse { None }
      case QueryParamP(k, BindP(bindName)) =>
        urlValue.params.find { case QueryParam(name, value) => k == name } flatMap { qp => Some(binds.updated(bindName, qp.value)) } orElse { None }
      case unknown @ _ => throw new RuntimeException(s"Unknown pattern $unknown")
    }
  }
}
