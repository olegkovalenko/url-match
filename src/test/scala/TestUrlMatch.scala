import org.scalatest.FunSuite

class TestUrlMatch extends FunSuite {
  // Array((query, Array((url, recognized)*))*)
  Array(
    ("""host(twitter.com); path(?user/status/?id);""",
      Array(
        ("http://twitter.com/bradfitz/status/562360748727611392",
          Some(Map("id" -> "562360748727611392", "user" -> "bradfitz"))))),

    ("""host(dribbble.com); path(shots/?id); queryparam(offset=?offset);""",
      Array(
        ("https://dribbble.com/shots/1905065-Travel-Icons-pack?list=users&offset=1",
          Some(Map("id" -> "1905065-Travel-Icons-pack", "offset" -> "1"))),
        ("https://dribbble.com/users/42",
          None))),

    ("""host(dribbble.com); path(shots/?id); queryparam(offset=?offset); queryparam(list=?type);""",
      Array(
        ("https://dribbble.com/shots/1905065-Travel-Icons-pack?list=users&offset=1",
          Some(Map("id" -> "1905065-Travel-Icons-pack", "offset" -> "1", "type" -> "users"))))),

    ("""host(dribbble.com); path(shots/?id); queryparam(offset=1); queryparam(list=?type);""",
      Array(
        ("https://dribbble.com/shots/1905065-Travel-Icons-pack?list=users&offset=1",
          Some(Map("id" -> "1905065-Travel-Icons-pack", "type" -> "users"))))),

    ("""host(?sitename); path(?user/status/?id);""",
      Array(
        ("http://twitter.com/bradfitz/status/562360748727611392",
          Some(Map("sitename" -> "twitter.com", "id" -> "562360748727611392", "user" -> "bradfitz")))))

  ).foreach {
    case (query, expectations) =>
      test(query) {
        val matcher = Matcher(query)
        for { (url, expected) <- expectations } assert(matcher.recognize(url) == expected)
      }
  }
}

